from setuptools import setup, find_packages
 
setup(name='pywasp',
      version='0.1',
      url='https://gitlab.com/devboost/pywasp.git',
      license='MIT',
      author='Anton Nadtoka',
      author_email='anton.nadtoka@yandex.ru',
      description='Python Paring Library',
      packages=find_packages(exclude=['tests']),
      long_description=open('README.md').read(),
      zip_safe=False)