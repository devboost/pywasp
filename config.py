import os
import pathlib
import configparser


class Config(object):
    dbHost = ""
    dbUser = ""
    dbPass = ""
    dbName = ""
    dbPort = 0
    workPath = ""

    def __init__(self):
        self.workPath = pathlib.Path(__file__).parent.resolve()

    def load(self, filename):
        if not os.path.exists(filename):
            return False

        config = configparser.ConfigParser()
        config.read(filename)

        self.dbHost = config['database']['host']
        self.dbUser = config['database']['user']
        self.dbPass = config['database']['pass']
        self.dbName = config['database']['name']
        self.dbPort = config['database']['port']
        self.workPath = config['common']['workdir']

    def save(self, filename):
        config = configparser.ConfigParser()

        config.add_section('database')
        config['database']['host'] = self.dbHost
        config['database']['user'] = self.dbUser
        config['database']['pass'] = self.dbPass
        config['database']['name'] = self.dbName
        config['database']['port'] = self.dbPort

        config.add_section('common')
        config['common']['workdir'] = self.workPath

        with open(filename, 'w') as file:
            config.write(file)